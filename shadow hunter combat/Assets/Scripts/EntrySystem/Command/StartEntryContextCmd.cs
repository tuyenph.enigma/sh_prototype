﻿using UnityEngine.SceneManagement;

namespace SH.EntrySystem.Command
{
    public class StartEntryContextCmd : strange.extensions.command.impl.Command
    {
        public override void Execute()
        {
            GoToHubScene();
        }

        private void GoToHubScene()
        {
            SceneManager.LoadScene("Hub");
        }
    }
}