﻿using System;

namespace SH.EntrySystem
{
    public class ContextView : strange.extensions.context.impl.ContextView
    {
        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            context = new SHEntryContext(this, true);
            context.Start();
        }
    }
}