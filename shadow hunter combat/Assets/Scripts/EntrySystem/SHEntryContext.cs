﻿using SH.EntrySystem.Command;
using SH.EntrySystem.Signal;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace SH.EntrySystem
{
    public class SHEntryContext : MVCSContext
    {
        public SHEntryContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping)
        {
        }

        protected override void mapBindings()
        {
            base.mapBindings();
            new CrossContextBindingConfig().MapBindings(injectionBinder,commandBinder,mediationBinder);

            commandBinder.Bind<StartEntryContextSignal>().To<StartEntryContextCmd>()
                .InSequence().Once();
            injectionBinder.Bind<SHEntryContext>().ToValue(this).ToSingleton().CrossContext();
        }

        public override void Launch()
        {
            injectionBinder.GetInstance<StartEntryContextSignal>().Dispatch();
        }
    }
}