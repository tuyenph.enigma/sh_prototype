// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "kokichi/Sprites/GrassSwaying"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_Speed ("MoveSpeed", Range(20,100)) = 100 // speed of the swaying
	    _Rigidness("Rigidness", Range(.1,10)) = 1 // lower makes it look more "liquid" higher makes it look rigid
	    _SwayMax("Sway Max", Range(0, .5)) = .2 // how far the swaying goes
	    _YOffset("Y offset", float) = 0.5// y offset, below this is no animation
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			uniform fixed _Speed;
			uniform fixed _Rigidness;
			uniform fixed _SwayMax;
			uniform fixed _YOffset;

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				fixed4 worldPos = mul (unity_ObjectToWorld, IN.vertex);
			    fixed x = sin(worldPos.x / _Rigidness + (_Time.x * _Speed)) * (worldPos.y - _YOffset);// x axis movements
			    fixed z = sin(worldPos.z / _Rigidness + (_Time.x * _Speed)) * (worldPos.y - _YOffset);// z axis movements
			    IN.vertex.x += step(0, worldPos.y - _YOffset) * x * _SwayMax;// apply the movement if the vertex's y above the YOffset
			    IN.vertex.z += step(0, worldPos.y - _YOffset) * z * _SwayMax;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _AlphaTex;
			float _AlphaSplitEnabled;

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
				if (_AlphaSplitEnabled)
					color.a = tex2D (_AlphaTex, uv).r;
#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
				return c;
			}
		ENDCG
		}
	}
}