﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextureFlash"
{
	Properties{
		_flashColor("Flash Color", Color) = (0,0,0,1)
		_flashValue("Flash Value", Range(0, 1)) = 0
		_flashScale("Flash Scale", Range(0, 1)) = 1
		[NoScaleOffset]_MainTex("Main Texture", 2D) = "white" {}
		_MainColor("Color", Color) = (1,1,1,1)
		_MainScale("Main Scale", Float) = 1.0
		_rimlightcolor ("Rim Light Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_rimlightscale ("Rim Light Scale", Float) = 1.0
	}
		SubShader{
		Tags{ "IgnoreProjector" = "True" "Queue" = "Transparent" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		LOD 100

		Blend One OneMinusSrcAlpha
		Cull Off
		ZWrite Off
		Lighting Off

		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
		uniform sampler2D _MainTex;
		uniform fixed4 _flashColor;
		uniform fixed _flashValue;
		uniform fixed _flashScale;
		uniform fixed4 _MainColor;
		uniform fixed _MainScale;
		uniform fixed4 _rimlightcolor;
		uniform fixed _rimlightscale;
		fixed4 _MainTex_TexelSize;

	struct VertexInput {
		fixed4 vertex : POSITION;
		fixed2 texcoord0 : TEXCOORD0;
		fixed4 vertexColor : COLOR;
	};

	struct VertexOutput {
		fixed4 pos : SV_POSITION;
		fixed2 uv0 : TEXCOORD0;
		fixed4 vertexColor : COLOR;
	};

	VertexOutput vert(VertexInput v) {
		VertexOutput o = (VertexOutput)0;
		o.uv0 = v.texcoord0;
		o.vertexColor = v.vertexColor;
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;
	}

	fixed4 frag(VertexOutput i) : COLOR{
		fixed4 rawColor = tex2D(_MainTex,i.uv0) * _MainColor ;
		fixed finalAlpha = (rawColor.a * i.vertexColor.a);
		fixed3 finalColor = lerp(_rimlightcolor.rgb * ceil(finalAlpha * _rimlightscale * 10), rawColor.rgb * i.vertexColor.rgb, finalAlpha);
		finalColor = lerp(finalColor, (_flashColor.rgb * finalAlpha), _flashValue * _flashScale);
		return fixed4(finalColor * _MainScale, finalAlpha);
	}
		ENDCG
	}
	}
		FallBack "Diffuse"
}