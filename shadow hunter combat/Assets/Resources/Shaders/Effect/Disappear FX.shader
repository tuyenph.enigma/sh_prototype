// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// - Unlit
// - Per-vertex gloss

Shader "kokichi/Fx/Disappear FX" {
Properties {
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_NoiseTex ("Noise tex", 2D) = "white" {}
	_FXColor("FXColor", Color) = (0,0.97,0.89,1)
	_TimeOffs("Time offs",float) = 0
	_Duration("Duration",float) = 2
	_Invert("Invert",float) = 0	
	_Border("Border Factor",float) = 4
	_Noise("Noise Factor",float) = 2
}

SubShader {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest
		#include "UnityCG.cginc"
		sampler2D _MainTex;
		sampler2D _NoiseTex;
		float4 _MainTex_ST;

		fixed4	_FXColor;
		float 	_TimeOffs;
		float 	_Duration;
		float	_Invert;
		float	_Border;
		float	_Noise;
		float	_GlobalTime;


		struct app_data
		{
			fixed4 vertex : POSITION;
			float3 normal : NORMAL;
			fixed4 texcoord : TEXCOORD0;
		};
		
		struct v2f
		{
			fixed4 pos : SV_POSITION;
			fixed2 tex : TEXCOORD0;
			fixed Threshold : TEXCOORD1;
		};
		
		
		v2f vert (app_data input)
		{
			v2f output;
			float  t = saturate((_TimeOffs + _GlobalTime) / _Duration); 	
			fixed3x3 modelMatrix = unity_ObjectToWorld;
			fixed3x3 modelMatrixInverse = unity_WorldToObject; 
			fixed3 normalDirection = normalize(mul(input.normal, modelMatrixInverse)); 
			output.tex = input.texcoord;
			output.pos = UnityObjectToClipPos(input.vertex);
			output.Threshold		= _Invert > 0 ? 1 - t : t;
			return output;
		}
	
		fixed4 frag (v2f i) : COLOR
		{
		
			fixed4	c			= tex2D (_MainTex, i.tex);
			fixed	noise 		= tex2D(_NoiseTex, i.tex * _Noise);
			fixed	killDiff	= noise - i.Threshold;
			fixed	border	= 1 - saturate(killDiff * _Border);
	
			border *= border;
			border *= border;


//			c.rgb *= i.diffuse;
			c.rgb += _FXColor.xyz * border;
//			c.rgb += dot(c.rgb,_FXColor.xyz) * border;
			
			c.a	= (noise > i.Threshold) * _FXColor.a;
			
			return c;
		}
		ENDCG
	}
}
}