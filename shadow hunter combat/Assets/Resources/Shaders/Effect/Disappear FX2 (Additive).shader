// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// - Unlit
// - Per-vertex gloss

Shader "kokichi/Fx/Disappear FX2 (Additive)" {
Properties {
	_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	_NoiseTex ("Noise tex", 2D) = "white" {}
	_FXColor("FXColor", Color) = (0,0.97,0.89,1)
	_TimeOffs("Time offs",float) = 0
	_Duration("Duration",float) = 2
	_Invert("Invert",float) = 0	
	_Mul("Multiple Factor", float) = 1
}

SubShader {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha One
	Cull Off
	ZWrite Off
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest
		#include "UnityCG.cginc"
		sampler2D _MainTex;
		sampler2D _NoiseTex;
		float4 _MainTex_ST;

		fixed4	_FXColor;
		float 	_TimeOffs;
		float 	_Duration;
		float	_Invert;
		float	_Mul;


		struct app_data
		{
			fixed4 vertex : POSITION;
			fixed4 texcoord : TEXCOORD0;
			fixed4 vertexColor : COLOR;
		};
		
		struct v2f
		{
			fixed4 pos : SV_POSITION;
			fixed2 tex : TEXCOORD0;
			fixed Threshold : TEXCOORD1;
			fixed4 vertexColor : COLOR;
		};
		
		
		v2f vert (app_data input)
		{
			v2f output;
			_TimeOffs = input.texcoord.z;
			float  t = saturate((_TimeOffs) / _Duration); 	
			output.tex = TRANSFORM_TEX(input.texcoord, _MainTex);
			output.pos = UnityObjectToClipPos(input.vertex);
			output.Threshold		= _Invert > 0 ? 1 - t : t;
			output.vertexColor = input.vertexColor;
			return output;
		}
	
		fixed4 frag (v2f i) : COLOR
		{
			fixed4	c			= tex2D (_MainTex, i.tex);
			fixed	noise 		= tex2D(_NoiseTex, i.tex);
			fixed	killDiff	= noise - i.Threshold;
			c.rgb = _Mul * c.rgb * _FXColor.rgb  * i.vertexColor.rgb;
			c.a *= i.vertexColor.a;
			c.a	*= noise > i.Threshold;
			return c;
		}
		ENDCG
	}
}
}