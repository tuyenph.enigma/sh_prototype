﻿Shader "Max/Blood_MeshMask"
{
	Properties
	{
		_Cutoff("Mask Clip Value", Float) = 0.5
		_MainTex("MainTex", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Emission("Emission", Range(0 , 1)) = 0
		_NormalValue("NormalValue", Float) = 1
		_FlowSpeed("FlowSpeed", Float) = 1
		[HideInInspector] _texcoord("", 2D) = "white" {}
		[HideInInspector] _texcoord2("", 2D) = "white" {}
		[HideInInspector] __dirty("", Int) = 1
	}

		SubShader
		{
			Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
			Cull Back
			CGPROGRAM
			#include "UnityShaderVariables.cginc"
			#pragma multi_compile_instancing
			#pragma surface surf Standard 
			struct Input
			{
				float2 uv_texcoord;
				float4 vertexColor : COLOR;
				float2 uv2_texcoord2;
			};

			uniform float _FlowSpeed;
			uniform float _NormalValue;
			uniform float4 _Color;
			uniform sampler2D _MainTex;
			uniform float _Emission;
			uniform float _Cutoff = 0.5;

			void surf(Input i , inout SurfaceOutputStandard o)
			{
				float mulTime35 = _Time.y * _FlowSpeed;
				float2 panner10 = (mulTime35 * float2(0,-0.2) + i.uv_texcoord);
				float3 appendResult25 = (float3(_NormalValue , _NormalValue , 1.0));
				float4 tex2DNode5 = tex2D(_MainTex, panner10);
				float4 temp_output_12_0 = (_Color * ((tex2DNode5.a + 1.0) * 0.5) * i.vertexColor);
				o.Emission = (temp_output_12_0 * _Emission).rgb;
				o.Alpha = 1;
				clip(step(((1.0 - i.uv2_texcoord2.x) * 1.01) , (((i.uv_texcoord.y * tex2DNode5.a) + tex2DNode5.a) / 2.0)) - _Cutoff);
			}

			ENDCG
		}
			Fallback "Diffuse"
				CustomEditor "ASEMaterialInspector"
}