// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "RyanShader/Rz_NoiseDissolve"
{
	Properties
	{
		_Color("Color", Color) = (1,0,0,1)
		_MainTex("MainTex", 2D) = "white" {}
		_Emission("Emission", Float) = 0
		_NormalValue("NormalValue", Float) = 0
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float2 uv2_texcoord2;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _NormalValue;
		uniform float4 _Color;
		uniform float _Emission;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode2 = tex2D( _MainTex, uv_MainTex );
			float3 appendResult18 = (float3(( tex2DNode2.g * _NormalValue ) , ( tex2DNode2.b * _NormalValue ) , 1.0));
			o.Normal = appendResult18;
			float4 temp_output_10_0 = ( _Color * ( ( tex2DNode2.r + 0.2 ) * 0.833 ) * i.vertexColor );
			o.Emission = ( temp_output_10_0 * _Emission ).rgb;
			float temp_output_15_0 = 0.5;
			float clampResult12 = clamp( i.uv2_texcoord2.x , 0.0 , 1.0 );
			o.Alpha = ( tex2DNode2.a * step( tex2DNode2.r , clampResult12 ) * i.vertexColor.a );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
