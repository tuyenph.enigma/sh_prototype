﻿namespace Artemis.System {

	#region Using statements

	using global::System;

	#endregion Using statements

	public abstract class EntitySystemWithTime : EntitySystem {


		public EntitySystemWithTime()
			: base() {
		}

		public override void Process() {
			Process(entityWorld.SimTimeDelta);
		}

		public abstract void Process(float deltaTime);

	}
}