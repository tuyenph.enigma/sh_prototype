﻿using strange.extensions.context.api;
using strange.extensions.injector.api;

namespace strange.extensions.context.impl
{
	public class FirstContext
	{
		public static IContext instance;
		public static ICrossContextInjectionBinder injectionBinder => (instance as MVCSContext).injectionBinder;

	}
}